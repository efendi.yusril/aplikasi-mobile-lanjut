import { StyleSheet, Text, View, TextInput, TouchableOpacity } from 'react-native'
import React, { Component } from 'react'

export class App extends Component {
  constructor(props) {
    super(props)
  
    this.state = {
      firstname: '',
      lastname: '',
      full: '',
    }
  }

  fullInput = () => {
    const firstname = this.state.firstname;
    const lastname = this.state.lastname;
    this.setState({full: firstname + " " + lastname});
  };


  render() {
    return (
      <View style={styles.container}>
        <View style={styles.boxInput}>
          <TextInput style={styles.input1} placeholder="First name" onChangeText={firstname => this.setState({firstname})} />
          <TextInput style={styles.input2} placeholder="Last name" onChangeText={lastname => this.setState({lastname})} />
        </View>

        <Text style={styles.full}>{this.state.full}</Text>

        <TouchableOpacity onPress={() => this.fullInput()}>
          <Text style={styles.textButton}>Full Name</Text>
        </TouchableOpacity>
      </View>
    )
  }
}

export default App

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
  },
  boxInput: {
    flexDirection: 'row',
    margin: 8
  },
  input1: {
    flex: 1,
    borderWidth: 1,
    borderColor: 'white',
    padding: 8,
    marginRight: 8,
  },
  input2: {
    flex: 1,
    borderWidth: 1,
    borderColor: 'white',
    padding: 8
  },
  full: {
    marginBottom: 10
  },
  textButton: {
    padding: 10,
    backgroundColor: 'yellow',
    color: 'black'
  }
})